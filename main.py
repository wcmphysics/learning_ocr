import gzip
import numpy as np
import matplotlib.pyplot as mpl



print("=== Optical Character Recoginition ===")


# read MNIST data set packed in gz format to numpy
f = gzip.open('MNIST-data/train-images-idx3-ubyte.gz','r')



image_size = 28
num_images = 5

f.read(16) # skip non-image info
buf = f.read(image_size * image_size * num_images)
data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
data = data.reshape(num_images, image_size, image_size, 1)


image = np.asarray(data[2]).squeeze()
mpl.imshow(image)
mpl.show()

#exit()

f = gzip.open('MNIST-data/train-labels-idx1-ubyte.gz','r')
f.read(8)
for i in range(0,50):   
    buf = f.read(1)
    labels = np.frombuffer(buf, dtype=np.uint8).astype(np.int64)
    print(labels)
